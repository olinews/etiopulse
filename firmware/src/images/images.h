#pragma once
#include "esp_attr.h"
/**
 * @brief All image to be displayed are declared from this header
 * 
 * To make a image follow these steps :
 * 1) Draw your image under GIMP (font DejaVu Sans 100px Bold) - Image size 200x117
 * 2) export image as a PBM ascii ans store them in the folder src/images
 * 3) Remember the width somewhere
 * 4) Apply the command: for i in *.pbm; do tail -n +4 $i | tr -d '\n' | fold -w 77 | sed  "s/0/_ /g" | sed "s/1/W /g" > ${i%.*}.txt;  done
 * 5) Add a new entry to images.h for each .txt file
 */

#define R 0x00,0x00,0xFF,
#define G 0x00,0xFF,0x00,
#define B 0xFF,0x00,0x00,
#define C 0xFF,0xFF,0x00,
#define M 0xFF,0x00,0xFF,
#define Y 0x00,0xFF,0xFF,
#define W 0xFF,0xFF,0xFF,
#define _ 0x00,0x00,0x00,

const uint8_t imgA[] = {
#include "images/A.txt"
};

const  uint8_t imgB[] = {
#include "images/B.txt"
};


const  uint8_t imgC[] = {
#include "images/C.txt"
};

const uint8_t imgD[] = {
#include "images/D.txt"
};


const uint8_t imgE[] = {
#include "images/E.txt"
};

const uint8_t imgF[] = {
#include "images/F.txt"
};

const uint8_t imgG[] = {
#include "images/G.txt"
};

const uint8_t imgZero[] = {
#include "images/zero.txt"
};

const uint8_t imgLight[] = {
#include "images/light.txt"
};
#undef R
#undef G
#undef B
#undef C
#undef M
#undef Y
#undef W
#undef _
const uint8_t *freqImages[] = {imgA, imgB, imgC, imgD, imgE, imgF, imgG};