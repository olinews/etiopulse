#pragma once
/**
 * @brief Hierarchical state machine
 *
 * See :
 * - https://en.wikipedia.org/wiki/UML_state_machine#Hierarchically_nested_states
 * - https://www.eventhelix.com/RealtimeMantra/HierarchicalStateMachine.htm
 *
 * This class handle events according to states.
 * Events are:
 *  - BTN_UP_SP				: Short press on button UP
 *	- BTN_MODE_SP				: Short press on button MODE
 *	- BTN_OK_SP					: Short press on button OK
 *	- BTN_DOWN_SP				: Short press on button DOWN
 *
 *	- BTN_RIGHT_LP				: Long press on button UP
 *	- BTN_MODE_LP				: Long press on button MODE
 *	- BTN_OK_LP					: Long press on button OK
 *	- BTN_LEFT_LP				: Long press on button DOWN
 *
 *	- END_OF_SCAN				: In auto mode need to switch to next frequency
 *
 *	- BATTERY_EMPTY				: To power off the device and save battery life
 *	- BATTERY_LOW				: To warn the user about battery low
 *	- BATTERY_STATUS			: To update the battery charge level
 */

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/timers.h"

#include "buttons.h"
#include "display.h"
#include "event_loop.h"
#include "freq_scanner.h"
#include "power_led.h"
#include "psu.h"
class Hsm
{
	//-----------------------------------------------------------------------------
	class BaseState
	{
	  public:
		explicit BaseState(Hsm *hsm);

		virtual void on_BTN_OK_LP(EventBits_t event);
		virtual void on_BTN_OK_SP(EventBits_t event);
		virtual void on_BTN_DOWN_SP(EventBits_t event);
		virtual void on_BTN_UP_SP(EventBits_t event);
		virtual void on_BTN_MODE_SP(EventBits_t event);
		virtual void on_BTN_LEFT_LP(EventBits_t event);
		virtual void on_BTN_RIGHT_LP(EventBits_t event);
		virtual void on_BTN_MODE_LP(EventBits_t event);
		virtual void on_END_OF_SCAN(EventBits_t event);
		// TODO TODEL virtual void on_BATTERY_EMPTY(EventBits_t event);
		// TODO TODEL virtual void on_BATTERY_LOW(EventBits_t event);
		virtual void on_BATTERY_STATUS(EventBits_t event);
		virtual void on_AUTO_POWER_OFF(EventBits_t event);

	  protected:
		Hsm *hsm_{nullptr};
	};
	friend BaseState;

	//-----------------------------------------------------------------------------
	class BatteryLowState : public BaseState
	{
	  public:
		BatteryLowState(Hsm *hsm) : BaseState(hsm) {}

		// virtual void on_BTN_OK_LP(EventBits_t event);                // event managed from BaseState
		virtual void on_BTN_OK_SP(EventBits_t event) override;	   // event to acknoledge the popup window
		virtual void on_BTN_DOWN_SP(EventBits_t event) override {} // event disabled
		virtual void on_BTN_UP_SP(EventBits_t event) override {}   // event disabled
		virtual void on_BTN_MODE_SP(EventBits_t event) override {} // event disabled
		// virtual void on_BATTERY_STATUS(EventBits_t event) override {} // event managed from BaseState
	};
	friend BatteryLowState;

	//-----------------------------------------------------------------------------
	class ManualState : public BaseState
	{
	  public:
		ManualState(Hsm *hsm) : BaseState(hsm) {}

		// virtual void on_BTN_OK_LP(EventBits_t event);                // event managed from BaseState
		virtual void on_BTN_DOWN_SP(EventBits_t event) override;
		virtual void on_BTN_UP_SP(EventBits_t event) override;
		virtual void on_BTN_OK_SP(EventBits_t event) override;
		virtual void on_BTN_MODE_SP(EventBits_t event) override;
	};
	friend ManualState;

	//-----------------------------------------------------------------------------
	class ScanReadyState : public BaseState
	{
	  public:
		ScanReadyState(Hsm *hsm) : BaseState(hsm) {}

		virtual void on_BTN_OK_SP(EventBits_t event) override;
		virtual void on_BTN_MODE_SP(EventBits_t event) override;
	};
	friend ScanReadyState;

	//-----------------------------------------------------------------------------
	class ScanRunningState : public ManualState
	{
	  public:
		ScanRunningState(Hsm *hsm) : ManualState(hsm) {}

		virtual void on_BTN_MODE_SP(EventBits_t event) override;
		virtual void on_END_OF_SCAN(EventBits_t event) override;
	};
	friend ScanRunningState;
	//-----------------------------------------------------------------------------

  public:
	explicit Hsm(EventLoop *eventLoop);
	virtual ~Hsm();
	void onEvent(EventBits_t event);

  private:
	BaseState *		  baseState_{nullptr};
	ManualState *	  manualState_{nullptr};
	ScanReadyState *  ScanReadyState_{nullptr};
	ScanRunningState *scanRunningState_{nullptr};

	BaseState * currentState_{baseState_};
	Psu			psu_;
	Buttons		buttons_;
	// Display		display_;
	Display2		display_;
	EventLoop * eventLoop_{nullptr};
	FreqScanner freqScanner_;
	PowerLed	powerLed_;

	void healthCheck_();
	void nextState_(BaseState *state) { currentState_ = state; }
};
