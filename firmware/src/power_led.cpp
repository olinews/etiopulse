#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "log_level.h"

#include "power_led.h"

PowerLed::PowerLed()
{
	/* Configure GPIO connected to IR LED PCB */
	gpio_config_t ioConfig{.pin_bit_mask = (1 << IrPowerGpio_) | (1 << IrDriveGpio_),
						   .mode = GPIO_MODE_OUTPUT,
						   .pull_up_en = GPIO_PULLUP_DISABLE,
						   .pull_down_en = GPIO_PULLDOWN_ENABLE,
						   .intr_type = GPIO_INTR_DISABLE};
	ESP_ERROR_CHECK(gpio_config(&ioConfig));
	gpio_set_level(IrPowerGpio_, 0);
	gpio_set_level(IrDriveGpio_, 0);

	ioConfig.pin_bit_mask = (1 << CONFIG_IR_SENSE_LEFT) | (1 << CONFIG_IR_SENSE_RIGHT);
	ioConfig.mode = GPIO_MODE_INPUT;
	ioConfig.pull_down_en = GPIO_PULLDOWN_ENABLE;
	ESP_ERROR_CHECK(gpio_config(&ioConfig));

	/* APP_CPU handles only timer ISR to switch on/off the IR led. */
	xTaskCreatePinnedToCore(&PowerLed::s_initBaseFreqTimer_, "init_tmr", 2048, this, 9, nullptr, APP_CPU_NUM);
}

esp_err_t PowerLed::setFreq(uint8_t freqIndex)
{
	if (lightIsOn_)
	{
		ESP_ERROR_CHECK(ledc_stop(channelConfig_.speed_mode, channelConfig_.channel, 0));
	}

	if (freqIndex > sizeof(freqValues_) / sizeof(freqValues_[0]))
	{
		return ESP_ERR_INVALID_ARG;
	}

	freqIndex_ = freqIndex;

	/* change LED_PWM frequency */
	timerConfig_.freq_hz = f100Values_[freqIndex];

	/* change timer frequency */
	ESP_ERROR_CHECK(timer_set_alarm_value(timerGroup_, timerIndex_, timerAlarmValues_[freqIndex]));

	// ESP_LOGI(__FILE__, "Switched to freq %s", freqNames_[freqIndex]);

	/* change base frequency */
	if (lightIsOn_)
	{
		ESP_ERROR_CHECK(ledc_timer_config(&timerConfig_));
		ESP_ERROR_CHECK(ledc_channel_config(&channelConfig_));
	}

	return ESP_OK;
}

esp_err_t PowerLed::nextFreq()
{
	freqIndex_ = (freqIndex_ + 1) % (sizeof(freqValues_) / sizeof(freqValues_[0]));
	return setFreq(freqIndex_);
}

esp_err_t PowerLed::prevFreq()
{
	freqIndex_ =
		((sizeof(freqValues_) / sizeof(freqValues_[0])) + freqIndex_ - 1) % (sizeof(freqValues_) / sizeof(freqValues_[0]));
	return setFreq(freqIndex_);
}

esp_err_t PowerLed::lightOn()
{
	if (0 == lightIsOn_)
	// ESP_LOGI(__FILE__, "light ON");
	enable_(); // power supply on IR PCB
	ESP_ERROR_CHECK(ledc_timer_config(&timerConfig_));
	ESP_ERROR_CHECK(ledc_channel_config(&channelConfig_));
	lightIsOn_ = 1;
	return ESP_OK;
}

esp_err_t PowerLed::lightOff()
{
	if (1 == lightIsOn_)
	{
		// ESP_LOGI(__FILE__, "light OFF");
		disable_(); // power supply on IR PCB

		ESP_ERROR_CHECK(ledc_stop(channelConfig_.speed_mode, channelConfig_.channel, 0));
		lightIsOn_ = 0;
	}
	return ESP_OK;
}

bool PowerLed::checkIrLed(gpio_num_t senseInput)
{
	enable_(); // power supply on IR PCB
	vTaskDelay(pdMS_TO_TICKS(1));
	gpio_set_level(IrDriveGpio_, 1);
	vTaskDelay(pdMS_TO_TICKS(1));
	auto senseLevelHigh = gpio_get_level(senseInput);
	gpio_set_level(IrDriveGpio_, 0);
	vTaskDelay(pdMS_TO_TICKS(1));
	auto senseLevelLow = gpio_get_level(senseInput);
	disable_(); // power supply on IR PCB
	// ESP_LOGI(__FILE__, "checkIrLed(%d) senseLevelHigh=%d, senseLevelLow=%d", senseInput, senseLevelHigh, senseLevelLow);
	return (1 == senseLevelHigh) && (0 == senseLevelLow);
}

void PowerLed::enable_()
{
	gpio_set_level(IrPowerGpio_, 1); // Enable V3.3 & VDD on IR PCB
}

void PowerLed::disable_()
{
	gpio_set_level(IrPowerGpio_, 0); // disable V3.3 & VDD on IR PCB
}

void PowerLed::s_initBaseFreqTimer_(void *arg)
{
	auto instance = reinterpret_cast<PowerLed *>(arg);
	instance->initBaseFreqTimer_();
	vTaskDelete(NULL);
}

void PowerLed::initBaseFreqTimer_()
{
	/* Select and initialize basic parameters of the timer */
	timer_config_t config = {.alarm_en = TIMER_ALARM_EN,
							 .counter_en = TIMER_START,
							 .intr_type = TIMER_INTR_LEVEL,
							 .counter_dir = TIMER_COUNT_UP,
							 .auto_reload = TIMER_AUTORELOAD_EN,
							 .divider = 2U}; // default clock source is APB (80Mhz) divided by 2 (40Mhz)

	ESP_ERROR_CHECK(timer_init(timerGroup_, timerIndex_, &config));

	/* Timer's counter will initially start from value below.
	   Also, if auto_reload is set, this value will be automatically reload on alarm */
	ESP_ERROR_CHECK(timer_set_counter_value(timerGroup_, timerIndex_, 0ULL));

	/* Configure the alarm value */
	ESP_ERROR_CHECK(timer_set_alarm_value(timerGroup_, timerIndex_, timerAlarmValues_[0]));

	/* Configure the interrupt on alarm. */
	ESP_ERROR_CHECK(timer_enable_intr(timerGroup_, timerIndex_));

	/*Register Timer interrupt handler, the handler is an ISR. The handler will be attached to the same CPU core that this
	 * function is running on. */
	timer_isr_register(timerGroup_, timerIndex_, s_onTimerIsr_, this, ESP_INTR_FLAG_IRAM, nullptr);
}

void IRAM_ATTR PowerLed::s_onTimerIsr_(void *arg)
{
	auto instance = reinterpret_cast<PowerLed *>(arg);
	instance->onTimerIsr_();
}

void IRAM_ATTR PowerLed::onTimerIsr_()
{
	timer_spinlock_take(timerGroup_);

	// ets_printf("Running on core %u\n", xPortGetCoreID()); // TIP: Use this statement to log from an ISR

	/* Retrieve the interrupt status and the counter value from the timer that reported the interrupt */
	uint32_t timer_intr = timer_group_get_intr_status_in_isr(timerGroup_);
	if (timer_intr & TIMER_INTR_T0)
	{
		timer_group_clr_intr_status_in_isr(timerGroup_, timerIndex_);
	}

	if (lightIsOn_)
	{ // manage blink
		if (irLedLevel_ == 0)
		{
			irLedLevel_ = 1;
			ESP_ERROR_CHECK(ledc_timer_config(&timerConfig_));
			ESP_ERROR_CHECK(ledc_channel_config(&channelConfig_));
		}
		else
		{
			irLedLevel_ = 0;
			ESP_ERROR_CHECK(ledc_stop(channelConfig_.speed_mode, channelConfig_.channel, 0));
		}
	}
	/* After the alarm has been triggered we need enable it again, so it is triggered the next time */
	timer_group_enable_alarm_in_isr(timerGroup_, timerIndex_);

	timer_spinlock_give(timerGroup_);
}
