The electronic design is done with Kicad (https://kicad.org/). If you do not have this software you can still view the electronic schematics in pdf format. To get a better fit between kicad files & git please follow [these instructions](https://jnavila.github.io/plotkicadsch/). Special thanks to [jnavila](https://github.com/jnavila) for his article about git/kicad cohabitation.

The device is composed of three sub-modules:
 1) The infrared LED driver
 2) The battery unit
 3) The microcontroller

