#pragma once

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"

class EventLoop
{
  public:
	/* the first event has the higher priority */

	static constexpr EventBits_t BTN_UP_SP{1 << 0};
	static constexpr EventBits_t BTN_MODE_SP{1 << 1};
	static constexpr EventBits_t BTN_OK_SP{1 << 2};
	static constexpr EventBits_t BTN_DOWN_SP{1 << 3};

	static constexpr EventBits_t BTN_RIGHT_LP{1 << 4};
	static constexpr EventBits_t BTN_MODE_LP{1 << 5};
	static constexpr EventBits_t BTN_OK_LP{1 << 6};
	static constexpr EventBits_t BTN_LEFT_LP{1 << 7};

	static constexpr EventBits_t END_OF_SCAN{1 << 8};	  // when scan finishes and doesn't loop-back
	static constexpr EventBits_t BATTERY_STATUS{1 << 9}; // need to refresh battery status

	static constexpr EventBits_t AUTO_POWER_OFF{1 << 10}; // the device need to shutdown after a period of inactivity

	/* Top events have higher priority among bottom events */
	// clang-format off
	const EventBits_t eventsMasks_[11] = {
		BTN_OK_LP,   
		BTN_OK_SP,	   
		BTN_DOWN_SP,	  
		BTN_UP_SP,   
		BTN_MODE_SP,
		BTN_LEFT_LP, 
		BTN_RIGHT_LP,   
		BTN_MODE_LP,	  
		END_OF_SCAN, 
		BATTERY_STATUS, 
		AUTO_POWER_OFF
	};
	// clang-format on

	const EventBits_t allEventsMask_ = BTN_OK_LP | BTN_OK_SP | BTN_DOWN_SP | BTN_UP_SP | BTN_MODE_SP | BTN_LEFT_LP |
									   BTN_RIGHT_LP | BTN_MODE_LP | END_OF_SCAN | BATTERY_STATUS |
									   AUTO_POWER_OFF;

	/* List of events that reset the autopower-off timer */
	const EventBits_t watchdogResetEventsMask_ =
		BTN_OK_LP | BTN_OK_SP | BTN_DOWN_SP | BTN_UP_SP | BTN_MODE_SP | BTN_LEFT_LP | BTN_RIGHT_LP | BTN_MODE_LP;

	explicit EventLoop();
	void		setEvents(const EventBits_t events);
	EventBits_t getNextEvent(TickType_t xTicksToWait);

  private:
	TimerHandle_t	   watchdogTimer_; // to manage auto power-off
	EventGroupHandle_t hEventGroup_ = xEventGroupCreate();

	uint32_t eventsPending_{};

	static void watchdogCallback_(TimerHandle_t arg) { static_cast<EventLoop *>(pvTimerGetTimerID(arg))->onWatchdog_(); }
	void		onWatchdog_();
};