#include "esp_err.h"
#include "log_level.h"
#include "esp_sleep.h"
#include "sdkconfig.h"
#include "buttons.h"

#define TOUCH_THRESH_NO_USE (0)
#define TOUCH_THRESH_PERCENT (66)

Buttons::Buttons(EventLoop &eventLoop) : eventLoop_(eventLoop)
{
	/* setup events managed from here */
	padsToEventsShortPress_[CONFIG_TOUCH_PAD_BTN_DOWN] = EventLoop::BTN_DOWN_SP;
	padsToEventsShortPress_[CONFIG_TOUCH_PAD_BTN_OK] = EventLoop::BTN_OK_SP;
	padsToEventsShortPress_[CONFIG_TOUCH_PAD_BTN_UP] = EventLoop::BTN_UP_SP;
	padsToEventsShortPress_[CONFIG_TOUCH_PAD_BTN_MODE] = EventLoop::BTN_MODE_SP;
	padsToEventsLongPress_[CONFIG_TOUCH_PAD_BTN_DOWN] = EventLoop::BTN_LEFT_LP;
	padsToEventsLongPress_[CONFIG_TOUCH_PAD_BTN_OK] = EventLoop::BTN_OK_LP;
	padsToEventsLongPress_[CONFIG_TOUCH_PAD_BTN_UP] = EventLoop::BTN_RIGHT_LP;
	padsToEventsLongPress_[CONFIG_TOUCH_PAD_BTN_MODE] = EventLoop::BTN_MODE_LP;
}

void Buttons::calibrate()
{
	ESP_LOGI(__FILE__, "Calibrating touch pad");
	const int nbSamples{50};
	for (auto &tpIndex : touchPadNums_)
	{
		ESP_ERROR_CHECK(touch_pad_config(tpIndex, 0));

		int64_t avg{0LL};
		for (int i = 0; i < nbSamples; i++)
		{
			uint16_t touchValue{0};
			ESP_ERROR_CHECK(touch_pad_read(tpIndex, &touchValue));
			avg += touchValue;
		}
		avg /= nbSamples;
		padInitValue_[tpIndex] = static_cast<uint32_t>(avg);
		ESP_ERROR_CHECK(touch_pad_config(tpIndex, (avg * TOUCH_THRESH_PERCENT) / 100));
		ESP_LOGI(__FILE__, "Buttons::Buttons() touchPadNum=%d avg=%d", tpIndex, padInitValue_[tpIndex]);
		vTaskDelay(pdMS_TO_TICKS(10));
	}

	for (auto &brt : btnRisingTime_)
	{
		brt = 0;
	}
	xTaskCreatePinnedToCore(&s_touchPadPollingTask_, "touch_polling", 2048, this, 5, nullptr, PRO_CPU_NUM);
}

void IRAM_ATTR Buttons::s_touchPadPollingTask_(void *arg)
{
	auto instance = reinterpret_cast<Buttons *>(arg);
	instance->touchPadPollingTask_();
}

void IRAM_ATTR Buttons::touchPadPollingTask_()
{
	for (;;)
	{
		/* read touch pad status */
		uint32_t padStatus{0};
		for (auto &tpIndex : touchPadNums_)
		{
			uint16_t touchValue{};
			touch_pad_read(tpIndex, &touchValue);
			// ESP_LOGI(__FILE__, "tpIndex=%d touchValue=%d", tpIndex, touchValue);
			if (touchValue < (padInitValue_[tpIndex] * TOUCH_THRESH_PERCENT) / 100)
			{ // set bit
				padStatus |= (0x01 << tpIndex);
			}
			else
			{ // reset bit
				padStatus &= ~(0x01 << tpIndex);
			}
		}

		/* analyse events to trigger */
		// ESP_LOGI(__FILE__, "lastStatus=%04x currStatus=%04x", lastPadStatus_, padStatus);
		for (auto &tpIndex : touchPadNums_)
		{
			if (((lastPadStatus_ & (0x01 << tpIndex)) == 0) && ((padStatus & (0x01 << tpIndex)) == 0))
			{ // button continue to be released
			  // ESP_LOGI(__FILE__, "btn=%d released", tpIndex);
			}
			else if (((lastPadStatus_ & (0x01 << tpIndex)) == 0) && ((padStatus & (0x01 << tpIndex)) != 0))
			{ // button rising edge
				btnRisingTime_[tpIndex] = xTaskGetTickCount();
				// ESP_LOGI(__FILE__, "btn=%d rising edge at %d", tpIndex, btnRisingTime_[tpIndex]);
			}
			else if (((lastPadStatus_ & (0x01 << tpIndex)) != 0) && ((padStatus & (0x01 << tpIndex)) != 0))
			{	// button continue to be pressed
				// ESP_LOGI(__FILE__, "btn=%d pressed", tpIndex);
				auto pressDurationMs = xTaskGetTickCount() - btnRisingTime_[tpIndex];
				if (CONFIG_LONG_PRESS_DURATION < pressDurationMs && pressDurationMs < CONFIG_LONG_PRESS_DURATION + 250)
				{ // this is a long press (2sec)
					ESP_LOGI(__FILE__, "btn=%d long press", tpIndex);
					eventLoop_.setEvents(padsToEventsLongPress_[tpIndex]);
					btnRisingTime_[tpIndex] = 0;
				}
			}
			else if (((lastPadStatus_ & (0x01 << tpIndex)) != 0) && ((padStatus & (0x01 << tpIndex)) == 0))
			{	// button falling edge
				/* do nothing if press duration is really short or really long. Send event short press otherwise */
				auto pressDuration = xTaskGetTickCount() - btnRisingTime_[tpIndex];
				// ESP_LOGI(__FILE__, "btn=%d falling edge after %d", tpIndex, pressDuration);
				if (pressDuration < 200)
				{ // this is a short press
					ESP_LOGI(__FILE__, "btn=%d short press", tpIndex);
					eventLoop_.setEvents(padsToEventsShortPress_[tpIndex]);
				}
				btnRisingTime_[tpIndex] = 0;
			}
		}
		lastPadStatus_ = padStatus;
		vTaskDelay(pdMS_TO_TICKS(25));
	}
}