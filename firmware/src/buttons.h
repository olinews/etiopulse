#pragma once

#include <driver/touch_pad.h>
#include "sdkconfig.h"
#include "event_loop.h"

class Buttons
{
public:
    Buttons(EventLoop& eventLoop);
	void calibrate();

private:
	const touch_pad_t touchPadNums_[4] = {
		static_cast<touch_pad_t>(CONFIG_TOUCH_PAD_BTN_DOWN), 
		static_cast<touch_pad_t>(CONFIG_TOUCH_PAD_BTN_OK), 
		static_cast<touch_pad_t>(CONFIG_TOUCH_PAD_BTN_UP),
		static_cast<touch_pad_t>(CONFIG_TOUCH_PAD_BTN_MODE)
	};
	EventBits_t		  padsToEventsShortPress_[TOUCH_PAD_MAX];
	EventBits_t		  padsToEventsLongPress_[TOUCH_PAD_MAX];

	EventLoop eventLoop_; // where to send button events

	uint32_t padInitValue_[TOUCH_PAD_MAX];	// to compute detection threshold

	uint32_t lastPadStatus_{};	// bit field with 1 bit of status for each pad

	TickType_t btnRisingTime_[TOUCH_PAD_MAX];	// when each each was pressed

	static void IRAM_ATTR s_touchPadPollingTask_(void *arg);
	void IRAM_ATTR		  touchPadPollingTask_();
};