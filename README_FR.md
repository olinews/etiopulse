|![Etiopulse](design/logo_etiopulse_128x128.png)  | ![oshw_facts](LICENSES/oshw_facts.png)|
|--|--|

*Etiopulse est un appareil qui émet un rayonnement infrarouge (940nm) pulsé en lumière non cohérente. Autrement dit, il ne fonctionne pas avec des diodes laser. Les fréquences de la lumière pulsées correspondent à celle établies par le [professeur Nogier](https://fr.wikipedia.org/wiki/Paul_Nogier). Ces fréquences stimulent une réaction de l'organisme, que Paul Nogier a nommé RAC - Réflexe Auriculo-Cardiaque.
Elles sont au nombre de sept, nommées de A0 à G0 (2,5Hz, 5 Hz, 10 Hz, 20 Hz, 40 Hz, 80 Hz, et 160 Hz). Cet appareil est destiné aux praticiens en [Etiomédecine](https://etiomedecine.fr/).*

---------------------
 
![Prototype v21.01](docs/img/v21.01/IMG_20211207_171421.jpg)

# Status

Ce projet est passé par plusieurs étapes dont trois prototypes fonctionnels. La version actuelle (21.01) est complètement opérationnelle et totalement décrite dans ce dépôt gitlab. L'étape suivante consiste à transformer le prototype fonctionnel en une version complètement aboutie.

# Compétence requises pour réaliser l'appareil

Ce projet a commencé comme un sujet d'étude personnel. Mon objectif étant de méler différentes disciplines, comme l'électronique et le travail du bois, que j'ai dû apprendre au fil du développement. Si vous souhaitez vous lancer dans la réalisation d'un Etiopulse ou d'un dérivatif il vous faudra maitriser (ou apprendre):
- Les bases fondamentales en électronique ;
- La brasure sur circuit imprimé SMD ;
- La programmation de microcontrolleur ESP32 ;
- La modélisation 3D pour la conception du boîtier ;
- Le pilotage de CNC pour l'usinage du boîtier.

# Un mot sur les licenses afférentes au projet

Une des contraintes que je me suis imposé, tout au long du projet, a été d'utiliser exclusivement des logiciels libres. Mettre ce projet dans le domaine du libre est donc ma manière de remercier tous les contributeurs aux outils que j'ai utilisé et d'adhérer à leur sens du partage. Plus précisément, je me suis appuyé sur :
- [Kubuntu](https://kubuntu.org/) sur ma station bureautique - Licence GPL
- [FreeCAD](https://www.freecadweb.org/?lang=fr) pour la modélisation 3D - LGPL2+ & CC-BY-3.0
- [Kicad](https://www.kicad.org/) pour la conception électronique - GPL
- [Visual Studio Code](https://code.visualstudio.com/) pour l'environnement de développement - Licence MIT
- [PlatformIO](https://platform.io/) comme framework de développement - Apache License 2.0
- [LovyanGFX](https://github.com/lovyan03/LovyanGFX) comme library graphique pour l'afficheur - BSD
- [ESP-IDF](https://github.com/espressif/esp-idf) pour la programmation ESP32 - Apache License 2.0
- [Debian](https://www.debian.org/) comme OS sur mon contrôleur CNC - [Diverses, libres selon Debian](https://fr.wikipedia.org/wiki/Principes_du_logiciel_libre_selon_Debian)
- [bCNC](https://github.com/vlachoudis/bCNC) comme application de pilotage de ma CNC -Licence MIT
- ...
- Mille pardons à ceux que j'oublie, s'ils me reviennent je les rajouterai ici.

C'est donc bien naturellement qu'il est certifié auprès de l'OSHW ([Open Source Hardware](https://www.oshwa.org/)) comme étant un projet entièrement open source depuis le 11 décembre 2021.

# !!! Quelques considérations sur la sécurité !!!

Cet appareil émet un puissant faisceau infra-rouge pulsé en longeur d'onde [940nm (IR-A)](https://fr.wikipedia.org/wiki/Infrarouge#D%C3%A9coupage_CIE). Les radiations infrarouges ont ceci de particulier que l'oeil humain ne les perçoit pas. Aucun réflexe pupillaire (contraction de la pupille) ni palpébral (clignement des yeux) n'est donc possible et les risques encourus sont des dommages irreversibles sur le tissu conjonctif, la cornée, la rétine, le cristallin, comme la [kératite](https://fr.wikipedia.org/wiki/K%C3%A9ratite), la [cataracte](https://fr.wikipedia.org/wiki/Cataracte_(maladie)), la [photophobie](https://fr.wikipedia.org/wiki/Photophobie), la [conjonctivite](https://fr.wikipedia.org/wiki/Conjonctivite), ...

La recommandation est donc la suivante :

---

 **Ne regardez pas en direction de l'appareil lorsqu'il est en cours de fonctionnement (led rouge allumée sur la face avant entre les deux leds infrarouge). Si vous utilisez l'appareil durant un traitement, demandez à votre patient de fermer les yeux ou de détourner le regard de l'Etiopulse.**

---

# organisation du projet

Le repository est divisé en plusieur sous-répertoires :

+ **cad** contient les fichiers FreeCAD et tout le design mécanique du boitier.
+ **design** contient le design graphique et artistique (ex logo).
+ **docs** contient à la fois les documentations techniques et la documentation utilisateur, les photos.
+ **firmaware** contient le micrologiciel développé pour l'ESP32.
+ **hardware** contient le projet Kicad, les schemas, layouts et librairies.
+ **LICENCES** contient la liste des licenses utilisées dans le projet.
+ **production** contient les fichiers gerber, le BOM et tout le nécessaire à la fabrication.
+ **simulation** contient tous les fichiers de simulation électrique ou autre ainsi que les résultats.


# Comment flasher le firmware

Le port USB de l'Etiopulse permet de recharger la batterie. À l'heure actuelle, il ne peut pas encore être utilisé pour flasher l'ESP32. Au lieu de cela un connecteur [ICSP](https://en.wikipedia.org/wiki/In-system_programming) se trouver sur le PCB MCU. Il vous faudra donc un programmateur type CP2101. Voici le miens:

![programmateur CP2101](docs/img/IMG_20211207_184031.jpg)

Ensuite il vous faut faire un petit circuit d'adaptation de signaux comme suit (partie de droite):

![circuit d'adaptation](docs/img/esp32-cp2102-programmer-schematic-for-espressif-esp32-esp8266.jpg)

L'ensemble ressemble à ceci pour moi:

![programmateur complet](docs/img/IMG_20211207_184604.jpg)

Le connecteur intermédiaire me sert à programmer d'autres montages ESP32. Il n'est pas utile dans le cas de l'Etiopulse.

Vous avouerez que ce n'est pas des plus pratique et je compte bien me pencher, un de ces jours, sur la programmation directe par le port USB de l'Etiopulse.

# Le boitier organique: un parti pris et assumé !

Le tout premier prototype de l'Etiopulse était réalisé dans un boîtier plastique. Le résultat était, selon moi, décevant. J'était dans une envie me connecter à l'étymologie du terme _boîtier_ en le fabricant, non pas en buis mais en chêne. Il m'a donc fallu apprendre la modélisation 3D ainsi que l'usinage à l'aide d'une CNC. C'est une activité passionnante mais très couteuse en temps.

![enclosure_body_internal](docs/img/v21.01/enclosure_body_inside.png)

![enclosure_body_external](docs/img/v21.01/enclosure_body_outside.png)

*In fine*, je peux soigner un design hors-norme et offrant une prise en main exceptionnelle de l'appareil.

![actual_enclosure](docs/img/v21.01/IMG_20211207_171723.jpg)