#include <cstring>


#include "log_level.h"
#include "psu.h"

bool Psu::PsuStatus::operator!=(const PsuStatus &other) const
{
	if (isStandby_ != other.isStandby_ || isCharging_ != other.isCharging_ || isUSB_ != other.isUSB_)
	{
		return true;
	}

	/* we notify only variations greater than 5% */
	if (chargePercent / 5 != other.chargePercent / 5 || batteryPercent / 5 != other.batteryPercent / 5)
	{
		return true;
	}

	return false;
}

Psu::Psu(EventLoop &eventLoop) : eventLoop_(eventLoop)
{
	/* init voltage measurement sampling arrays */
	std::memset(mvBatts_, 0, sizeof(mvBatts_));
	std::memset(mvProgs_, 0, sizeof(mvProgs_));

	/* init battery voltage measurement output */
	gpio_pad_select_gpio(CONFIG_BATT_CHECK);
	gpio_set_direction(static_cast<gpio_num_t>(CONFIG_BATT_CHECK), GPIO_MODE_OUTPUT);

	/* init inputs for TP4056 CHARGE & STANDBY */
	gpio_pad_select_gpio(CONFIG_BATT_CHARGE);
	gpio_set_direction(static_cast<gpio_num_t>(CONFIG_BATT_CHARGE), GPIO_MODE_INPUT);
	gpio_pad_select_gpio(CONFIG_BATT_STANDBY);
	gpio_set_direction(static_cast<gpio_num_t>(CONFIG_BATT_STANDBY), GPIO_MODE_INPUT);

	/* init ADC channels */
	static constexpr auto adcWidth{ADC_WIDTH_BIT_12};
	static constexpr auto adcAtnBatt{ADC_ATTEN_DB_11};
	static constexpr auto adcAtnCharge{ADC_ATTEN_DB_6};
	adc1_config_width(adcWidth);
	adc1_config_channel_atten(adcBattSenseChannel_, adcAtnBatt);
	adc1_config_channel_atten(adcChargeSenseChannel_, adcAtnCharge);
	adcBattCalibration_ = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
	esp_adc_cal_characterize(adcUnit_, adcAtnBatt, adcWidth, adcVref_, adcBattCalibration_);
	adcChargeCalibration_ = (esp_adc_cal_characteristics_t *)calloc(1, sizeof(esp_adc_cal_characteristics_t));
	esp_adc_cal_characterize(adcUnit_, adcAtnCharge, adcWidth, adcVref_, adcChargeCalibration_);
}

void Psu::start()
{
	/* pre-fill voltages samplings tables */
	for (auto index{0}; index < maxMeasures_; index++)
	{
		onStatus_();
	}

	/* init timer for battery sensing */
	statusTimer_ = xTimerCreate("psu_status",		   // Just a text name, not used by the RTOS kernel.
								statusSamplingPeriod_, // The timer period in ms, must be greater than 0.
								pdTRUE,				   // The timers will auto-reload itself when it expire.
								this,				   // This pointer passed to the callback
								statusCallback_		   // Each timer calls the same callback when it expires.
	);
	xTimerStart(statusTimer_, 0); // timer start immediately
}

void Psu::onStatus_()
{
	// ESP_LOGD(__FILE__, "Psu::onStatus_()");

	lastStatus_ = currStatus_;

	/* measure voltage on the battery */
	ESP_ERROR_CHECK(gpio_set_level(static_cast<gpio_num_t>(CONFIG_BATT_CHECK), 0)); // enable battery voltage divider
	vTaskDelay(pdMS_TO_TICKS(100));
	int adcReadingBatt{adc1_get_raw((adc1_channel_t)adcBattSenseChannel_)};
	ESP_ERROR_CHECK(gpio_set_level(static_cast<gpio_num_t>(CONFIG_BATT_CHECK), 1)); // disable battery voltage divider
	mvBatts_[measureIndex_] = esp_adc_cal_raw_to_voltage(adcReadingBatt, adcBattCalibration_);
	// ESP_LOGD(__FILE__, "adcReadingBatt=%d mvBatt=%d", adcReadingBatt, mvBatts_[measureIndex_]);

	/* get charging & standby status (logic is negative because of the open collector on TP4056) */
	auto tp4056Charge = gpio_get_level(static_cast<gpio_num_t>(CONFIG_BATT_CHARGE));
	auto tp4056Standby = gpio_get_level(static_cast<gpio_num_t>(CONFIG_BATT_STANDBY));
	currStatus_.isCharging_ = (tp4056Charge == 0) && (tp4056Standby == 1);
	currStatus_.isStandby_ = (tp4056Charge == 1) && (tp4056Standby == 0);
	currStatus_.isUSB_ = (currStatus_.isCharging_ || currStatus_.isStandby_);

	/* measure voltage on TP4056 Pin 2-PROG. This is usefull only when USB is connected (5V to VCC on TP4056) */
	if (currStatus_.isUSB_)
	{
		int adcReadingCharge{adc1_get_raw((adc1_channel_t)adcChargeSenseChannel_)};
		mvProgs_[measureIndex_] = esp_adc_cal_raw_to_voltage(adcReadingCharge, adcChargeCalibration_);
		// ESP_LOGD(__FILE__, "adcReadingCharge=%d mvCharge=%d adcReadingBatt=%d mvBatts_=%d", adcReadingCharge, mvProgs_[measureIndex_], adcReadingBatt, mvBatts_[measureIndex_]*2);
	}
	else
	{
		mvProgs_[measureIndex_] = 0;
	}

	/* reset averaging algorithm if USB is plugged or unplugged
	   https://gitlab.com/olinews/etiopulse/-/issues/18 */
	if (lastStatus_.isUSB_ != currStatus_.isUSB_)
	{
		auto mvBatt = mvBatts_[measureIndex_];
		auto mvProg = mvProgs_[measureIndex_];
		for (auto index{0}; index < maxMeasures_; index++)
		{
			mvBatts_[index] = mvBatt;
			mvProgs_[index] = mvProg;
		}
	}

	/* compute mean voltages on battery & charge */
	uint32_t mvBatt{0};
	uint32_t mvProg{0};
	for (auto index{0}; index < maxMeasures_; index++)
	{
		mvBatt += mvBatts_[index];
		mvProg += mvProgs_[index];
	}
	mvBatt *= 2; // because of the voltage divider that return half the actual voltage of the battery
	mvBatt /= maxMeasures_;
	mvProg /= maxMeasures_;

	/* convert battery milivolts to percentage */
	if (mvBatt <= mVBattEmpty_)
	{
		currStatus_.batteryPercent = 0;
	}
	else if (mvBatt >= mvBattFull_)
	{
		currStatus_.batteryPercent = 100;
	}
	else
	{
		currStatus_.batteryPercent = (100 * (mvBatt - mVBattEmpty_)) / (mvBattFull_ - mVBattEmpty_);
	}

	/* convert prog milivolts (TP4056::PROG pin) to percentage */
	if (mvProg <= mvProgEnd_)
	{
		currStatus_.chargePercent = 100;
	}
	else if (mvProg >= mVProgBegin_)
	{
		currStatus_.chargePercent = 0;
	}
	else
	{
		if (mvBatt < 4.3f)
		{	// use batterie voltage to estimate charge progression - first part of the charge cycle: constant current
			static const std::map<uint32_t, uint8_t> CHARGE_VS_VBATT {{2850, 0}, {3000, 5}, {3150, 10}, {3250, 15}, {3380, 20}, {3500, 25}, {3630, 30}, {3750, 40}, {3880, 40}, {4000, 45}, {4050, 50}, {4100, 60}, {4250, 65}, {4300, 75}};
			auto it = CHARGE_VS_VBATT.upper_bound(mvBatt);
			currStatus_.chargePercent = it->second;
		}
		else
		{	// use vprog voltage to estimate charge progression - second part of the charge cycle: constant voltage
			static const std::map<uint32_t, uint8_t> CHARGE_VS_VPROG {{600, 70}, {500, 75}, {400, 80}, {300, 85}, {200, 90}, {100, 95}, {0, 100}};
			auto it = CHARGE_VS_VPROG.upper_bound(mvProg);
			currStatus_.chargePercent = it->second;
		}

		// currStatus_.chargePercent = (100 * (mVProgBegin_ - mvProg)) / (mVProgBegin_ - mvProgEnd_);
	}

	/* cycle to next sampling position */
	measureIndex_++;
	measureIndex_ %= maxMeasures_;
	// ESP_LOGD(__FILE__, "isCharging_=%d, isStandby_=%d, isUSB_=%d, mvBat_=%d(%d %%), mvProg_=%d (%d %%) measureIndex=%d",
	// 		 currStatus_.isCharging_, currStatus_.isStandby_, currStatus_.isUSB_, mvBatt, currStatus_.batteryPercent, mvProg,
	// 		 currStatus_.chargePercent, measureIndex_);

	/* notify HSM in case of any change in PSU status */
	if (lastStatus_ != currStatus_)
	{
		// ESP_LOGD(__FILE__, "PSU status change : Notify");
		eventLoop_.setEvents(EventLoop::BATTERY_STATUS);
	}
}