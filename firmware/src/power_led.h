#pragma once
#include "driver/gpio.h"
#include "driver/ledc.h"
#include "driver/timer.h"
#include "esp_err.h"
#include "sdkconfig.h"

/* Frequencies found by Paul Nogier
Name  Base freq(Hz) Period(ms)  Timer value  Pulse Freq(Hz)
A0   	  2,28		438,60		8770752		  228
B0    	  4,56		219,30		4385376		  456
C0    	  9,12		109,65		2192688		  912
D0   	 18,24		 54,82		1096344		 1824
E0   	 36,48		 27,41		 548172		 3648
F0   	 72,96		 13,71		 274086		 7296
G0		145,92		  6,85		 137043		14592

										<---- Nogier period (eg 2.28Hz)  ------>

														   <- Duty cycle  50% ->
										<---> Pulse (light on/off) is Nogier period / 100 & duty cycle 50%
 _   _   _   _   _                       _   _   _   _   _                       _   _   _   _   _
/ \_/ \_/ \_/ \_/ \_____________________/ \_/ \_/ \_/ \_/ \_____________________/ \_/ \_/ \_/ \_/ \_____________________
*/

class PowerLed
{
  public:
	explicit PowerLed();

	/**
	 * @brief Set the desired frequency among A0 to G0
	 *
	 * @param freqIndex	index of the frequency selected in range [0..6]
	 * @return esp_err_t ESP_ERR_INVALID_ARG if freqIndex is outside the range [0..6]; ES_OK otherwise
	 */
	esp_err_t setFreq(uint8_t freqIndex);

	const char *getCurrentFreqName() const { return freqNames_[freqIndex_]; }
	uint8_t		getCurrentFreqIndex() const { return freqIndex_; }
	esp_err_t	nextFreq();
	esp_err_t	prevFreq();
	esp_err_t	lightOn();
	esp_err_t	lightOff();
	int			lightStatus() { return lightIsOn_; } // 1: light is on; 0: light is off
	bool		checkIrLed(gpio_num_t senseInput);

  private:
	/**
	 * @brief Port connected to the IR LED hardware
	 *
	 * This GPIO port is configured from idf.py menuconfig > Etiopulse Configuration > IR_LIGHT_GPIO
	 *
	 */
	static constexpr gpio_num_t IrDriveGpio_{static_cast<gpio_num_t>(CONFIG_IR_DRIVE)};
	static constexpr gpio_num_t IrPowerGpio_{static_cast<gpio_num_t>(CONFIG_IR_POWER)};
	
	/**
	 * @brief Led PWM timer configuration for pulse light (100 time the base freq)
	 *
	 * See ̣ESP technical reference manual § 14. LED_PWM
	 */
	ledc_timer_config_t timerConfig_{.speed_mode = LEDC_HIGH_SPEED_MODE,
									 .duty_resolution = LEDC_TIMER_8_BIT,
									 .timer_num = LEDC_TIMER_0,
									 .freq_hz = 228,
									 .clk_cfg = LEDC_AUTO_CLK};

	/**
	 * @brief Led PWM channel configuration for pulse light (100 time the base freq)
	 *
	 * See ̣ESP technical reference manual § 14. LED_PWM
	 */
	ledc_channel_config_t channelConfig_{.gpio_num = IrDriveGpio_,
										 .speed_mode = LEDC_HIGH_SPEED_MODE,
										 .channel = LEDC_CHANNEL_0,
										 .intr_type = LEDC_INTR_DISABLE,
										 .timer_sel = LEDC_TIMER_0,
										 .duty = 128, // means 50%
										 .hpoint = 0};

	/**
	 * @brief Timer used for base freq generation
	 *
	 * We use the first timer of the first group TIMG0_T0.
	 * See ̣ESP technical reference manual § 18.1 64 bits timers - Introduction
	 */
	const timer_group_t timerGroup_{TIMER_GROUP_0};
	const timer_idx_t	timerIndex_{TIMER_0};

	int irLedLevel_{0}; // 0=LED_PWM stopped; 1=LED PWM running
	int lightIsOn_{0};	// 0 light is off; 1 light is on

	/**
	 * @brief Timer to manage the base freq
	 *
	 * setup the high speed timer that handle the base frequency in range [2.28..145.92] Hz
	 * See ̣ESP technical reference manual § 18. 64-bit Timers
	 */
	static void IRAM_ATTR s_initBaseFreqTimer_(void *arg);
	void				  initBaseFreqTimer_();

	/**
	 * @brief Timer interrupt for the base freq management
	 *
	 * This interrupt is called twice per period. First to switch on the light; second to switch off the light.
	 * Since the interrupt handler is a member function we need a static function (timerGroup0Isr_) called first to delegate to
	 * the member function (onTimerIsr_).
	 * @param arg object pointer to PowerLed instance
	 */
	static void IRAM_ATTR s_onTimerIsr_(void *arg);
	void IRAM_ATTR		  onTimerIsr_();

	void enable_();	 // power up IR PCB
	void disable_(); // shutdown IR PCB

	uint8_t		   freqIndex_{0};
	const char *   freqNames_[7] = {"A0", "B0", "C0", "D0", "E0", "F0", "G0"};
	const double   freqValues_[7] = {2.28, 4.56, 9.12, 18.24, 36.48, 72.96, 145.92};
	const uint32_t f100Values_[7] = {228, 456, 912, 1824, 3648, 7296, 14592};
	const uint64_t timerAlarmValues_[7] = {8770752, 4385376, 2192688, 1096344, 548172, 274086, 137043};
};