|![Etiopulse](design/logo_etiopulse_128x128.png)  | [![oswh_id](LICENSES/oshwa-2.5-single-dual.svg)](https://certification.oshwa.org/fr000018.html)|![oshw_facts](LICENSES/oshw_facts.png) |
|--|--|--|


*Etiopulse is a device that emits an infrared beam (940nm) pulsed in non-coherent light. In other words, it does not work with laser diodes. The frequencies of the pulsed light correspond to those established by [Professor Nogier] (https://fr.wikipedia.org/wiki/Paul_Nogier). These frequencies stimulate a reaction of the organism, which Paul Nogier named RAC - Auriculo-Cardiac Reflex.
There are seven of them, named from A0 to G0 (2.5 Hz, 5 Hz, 10 Hz, 20 Hz, 40 Hz, 80 Hz, and 160 Hz). This device is intended for practitioners in [Etiomedicine](https://etiomedecine.fr/).*

---------------------
 
![Prototype v21.01](docs/img/v21.01/IMG_20211207_171421.jpg)

# Status

This project has gone through several stages including three working prototypes. The current version (21.01) is fully operational and fully described in this gitlab repository. The next step is to transform the functional prototype into a fully functional version.

# Skills required to build the device

This project started as a personal study subject. My goal was to mix different disciplines, such as electronics and woodworking, which I had to learn as I developed it. If you want to start making an Etiopulse or a derivative you will need to master (or learn):
- Basic electronics ;
- Soldering on SMD printed circuit;
- ESP32 microcontroller programming;
- 3D modeling for the design of the package;
- CNC router for machining the package.

# A word about the project's licenses

One of the constraints that I imposed myself, throughout the project, was to use exclusively free software. Putting this project in the free domain is my way of thanking all the contributors to the tools I used and to adhere to their sense of sharing. More precisely, I relied on :
- [Kubuntu](https://kubuntu.org/) on my office station - GPL license
- [FreeCAD](https://www.freecadweb.org/?lang=fr) for 3D modeling - LGPL2+ & CC-BY-3.0
- [Kicad](https://www.kicad.org/) for electronic design - GPL
- Visual Studio Code](https://code.visualstudio.com/) for the development environment - MIT License
- [PlatformIO](https://platform.io/) as a development framework - Apache License 2.0
- [LovyanGFX](https://github.com/lovyan03/LovyanGFX) as a graphic library for the display - BSD
- [ESP-IDF](https://github.com/espressif/esp-idf) for ESP32 programming - Apache License 2.0
- [Debian](https://www.debian.org/) as OS on my CNC controller - [Miscellaneous, free according to Debian](https://fr.wikipedia.org/wiki/Principes_du_logiciel_libre_selon_Debian)
- [bCNC](https://github.com/vlachoudis/bCNC) as an application to control my CNC - MIT License
- ...
-My apologies to those I forget!

It is thus quite naturally that it is certified by OSHW ([Open Source Hardware](https://www.oshwa.org/)) as a truly open source project since December 11th 2021.

# !!! Some security considerations !!!

This device emits a powerful pulsed infrared beam in wavelength [940nm (IR-A)](https://fr.wikipedia.org/wiki/Infrarouge#D%C3%A9coupage_CIE). Infrared radiations have this particularity that the human eye does not perceive them. No pupillary reflex (pupil contraction) or palpebral reflex (blinking) is therefore possible and the risks incurred are irreversible damage to the connective tissue, cornea, retina, lens, such as [keratitis](https://fr.wikipedia.org/wiki/K%C3%A9ratite), [cataract](https://fr.wikipedia.org/wiki/Cataracte_(disease)), [photophobia](https://fr.wikipedia.org/wiki/Photophobie), [conjunctivitis](https://fr.wikipedia.org/wiki/Conjonctivite), ...

The recommendation is therefore as follows:

---

 **Do not look at the device when it is in operation (red led lit on the front between the two infrared leds). If you are using the device during a treatment, ask your patient to close his eyes or to look away from the Etiopulse.**

---

# organization of the project

The repository is divided into several subdirectories:

+ **cad** contains the FreeCAD files and all the mechanical design of the case.
+ **design** contains the graphic and artistic design (e.g. logo).
+ **docs** contains both technical and user documentation, photos.
+ **firmaware** contains the firmware developed for the ESP32.
+ **hardware** contains the Kicad project, schemas, layouts and libraries.
+ **LICENCES** contains the list of licenses used in the project.
+ **production** contains the gerber files, the BOM and everything needed for the production.
+ **simulation** contains all the electrical and other simulation files as well as the res

# How to flash the firmware

The USB port of the Etiopulse is used to recharge the battery. It cannot be used to flash the ESP32 yet. Instead an [ICSP](https://en.wikipedia.org/wiki/In-system_programming) connector is located on the MCU PCB. So you will need an CP2101 like programmer. Here is mine:

![programmateur CP2101](docs/img/IMG_20211207_184031.jpg)

Then you have to make a small signal adaptation circuit as follows (right part):

![matching circuit](docs/img/esp32-cp2102-programmer-schematic-for-espressif-esp32-esp8266.jpg)

The whole thing looks like this to me:

![programmateur complet](docs/img/IMG_20211207_184604.jpg)

I use the intermediate connector to program other ESP32 assemblies. It is not useful in the case of the Etiopulse.

You'll admit that it's not the most practical and I'm planning to look into direct programming via the Etiopulse's USB port one of these days.

# The organic box: a choice and a commitment!

The very first prototype of the Etiopulse was made in a plastic case. The result was, in my opinion, disappointing. I wanted to connect with the etymology of the term _box_ (derivated from *boxwood* ) by making it not out of boxwood but out of oak. So I had to learn 3D modeling and machining with a CNC router. It is an exciting activity but very expensive in time.

![enclosure_body_internal](docs/img/v21.01/enclosure_body_inside.png)

![enclosure_body_external](docs/img/v21.01/enclosure_body_outside.png)

*In fine*, I can take care of an outstanding design and offering an exceptional grip of the device.

![actual_enclosure](docs/img/v21.01/IMG_20211207_171723.jpg)
