Les licences utilisés dans ce projet sont :

- Hardware : [CERN Open Hardware Licence Version 2 - Strongly Reciprocal](https://cern-ohl.web.cern.ch/)
- Software : License [GPL-V.3.0] (https://www.gnu.org/licenses/gpl-3.0.fr.html)
- Documentation : [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)
