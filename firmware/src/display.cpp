#include "display.h"
#include "sdkconfig.h"

Display2::Display2()
{
    /* init OLED power management IO */
	gpio_pad_select_gpio(CONFIG_OLED_PWR);
	gpio_set_direction(static_cast<gpio_num_t>(CONFIG_OLED_PWR), GPIO_MODE_OUTPUT);
	gpio_set_level(static_cast<gpio_num_t>(CONFIG_OLED_PWR), 0); // enable power on OLED screen
	vTaskDelay(pdMS_TO_TICKS(500));

    /* configure I2C bus */
    auto busConfig = _bus_instance.config();    // Obtient la structure pour la configuration du bus.
    busConfig.i2c_port    = 1;          // Sélectionnez le port I2C à utiliser (0 or 1)
    busConfig.freq_write  = 400000;     // Horloge au moment de la transmission
    busConfig.freq_read   = 400000;     // Horloge à la réception
    busConfig.pin_sda     = CONFIG_OLED_SDA;         // Numéro de broche connectant le SDA
    busConfig.pin_scl     = CONFIG_OLED_SCK;         // Numéro de broche connectant SCL
    busConfig.i2c_addr    = 0x3C;       // Adresse du périphérique I2C
    _bus_instance.config(busConfig);    // La valeur définie est répercutée sur le bus.
    _panel_instance.setBus(&_bus_instance);      // Définit le bus vers le panneau.

    auto panelConfig = _panel_instance.config (); // Récupère la structure des paramètres du panneau d'affichage.
    panelConfig.pin_cs = -1; // Numéro de broche auquel CS est connecté (-1 = désactiver)      
    panelConfig.pin_rst   =    -1;  // Numéro de broche auquel RST est connect (-1 = disable)
    panelConfig.pin_busy = -1 ; // Numéro de broche auquel BUSY est connecté (-1 = désactiver)

    /* Les valeurs de réglage suivantes sont définies sur des valeurs initiales générales pour chaque panneau.
    Veuillez donc commenter tous les éléments inconnus et les essayer. */
    panelConfig.memory_width = 132 ; // Largeur maximale prise en charge par le pilote I2C			
    panelConfig.memory_height = 64 ; // Hauteur maximale prise en charge par le pilote I2C
    panelConfig.panel_width = 128 ; // Largeur réellement affichable
    panelConfig.panel_height = 64; // Hauteur qui peut réellement être affichée
    panelConfig.offset_x = 0; // Quantité de décalage dans la direction X du panneau
    panelConfig.offset_y = 0; // Quantité de décalage dans la direction Y du panneau
    panelConfig.offset_rotation = 2; // Décalage de la valeur dans le sens de rotation 0 ~ 7 (4 ~ 7 est à l'envers)
    panelConfig.dummy_read_pixel = 8; // Nombre de bits de lecture factice avant la lecture du pixel
    panelConfig.dummy_read_bits = 1; // Nombre de bits de lecture factice avant de lire des données autres que des pixels
    panelConfig.readable = true ; // Mettre à true si les données peuvent être lues
    panelConfig.invert = false ; // Mettre à true si la lumière et l'obscurité du panneau sont inversées
    panelConfig.rgb_order = false; // Mis à true si le rouge et le bleu du panneau sont intervertis
    panelConfig.dlen_16bit = false ; // Définir sur true pour les panneaux qui envoient la longueur des données en unités de 16 bits
    panelConfig.bus_shared = false ; // Mis à true si le bus est partagé avec la carte SD (le contrôle du bus est effectué avec drawJpgFile etc.)

    _panel_instance.config(panelConfig);

    setPanel(& _panel_instance); // Définit le panneau à utiliser.	

}

Display2::~Display2()
{
	gpio_set_level(static_cast<gpio_num_t>(CONFIG_OLED_PWR), 1); // disable power on OLED display
}

// splash screen
void Display2::drawWelcome()
{
    clear();
    setTextSize(1);
    setColor(TFT_WHITE);
	setFont(&fonts::FreeSansBold9pt7b);

    const char *msg1{"Etiopulse"};
	drawString(msg1, (getPanel()->width() - textWidth(msg1)) / 2, 10);

	const char *msg2{"v21.01"};
	drawString(msg2, (getPanel()->width() - textWidth(msg2)) / 2, 30);
    
    // drawRect(2,0, 126, 64); // Maximum drawable area
}

void Display2::drawGoodbye()
{
    clear();

    setTextSize(1.0);
    setFont(&fonts::FreeSansBold9pt7b);
	const char *msg{"Au revoir."};
	drawString(msg, (getPanel()->width() - textWidth(msg)) / 2, 30);
}

void Display2::drawProgression(int progression) // 0% to 100%
{
    setColor(TFT_WHITE);
	drawRect(13, 49, 102, 12);
	fillRect(14 + lastProgression_, 50, progression - lastProgression_, 10);
	
    lastProgression_ = progression;
} 

void Display2::drawHealthStatus(bool leftLed, bool rightLed)
{
	setFont(&fonts::FreeSansBold9pt7b);
    
    std::string msg;
    msg = "IR Led 1: ";
    msg += (leftLed ? "OK" : "PB!");
    drawString(msg.c_str(), 5, 15);

    msg = "IR Led 2: ";
    msg += (rightLed ? "OK" : "PB!");
    drawString(msg.c_str(), 5, 35);
}

void Display2::drawBatteryStatus(int chargePercent, int batteryPercent, bool isCharging, bool isUSB)
{
	int32_t x0{90};
	int32_t y0{30};
	int32_t w{20};
	int32_t h{8};
    setTextSize(0.5);

    auto percent = (isCharging ? chargePercent : batteryPercent);

    setFont(&fonts::FreeSans9pt7b);

	/* clear power status area */
	fillRect(x0 - 30, y0 - 30, 60, 25, TFT_BLACK);

    // FOR DEBUGGING
    // setCursor(x0, y0-5);
	// printf("%d%% ", percent);

    setColor(TFT_WHITE);

	/* draw power status on display */
	if (isUSB)
	{ // draw an electric plug
		fillRect(x0 - 25, y0 - 16, 15, 2);		   // cable
		fillRoundRect(x0 - 15, y0 - 19, 10, h, 3); // plug rear
		fillRect(x0 - 10, y0 - 19, 5, h);		   // plug front
		fillRect(x0 - 5, y0 - 18, 4, 1);		   // pin 1
		fillRect(x0 - 5, y0 - 13, 4, 1);		   // pin 2
        
        setTextSize(0.8);
        setFont(&fonts::FreeSans9pt7b);
	    drawString("75\%", x0 + 2, y0 - 20);
	}
    else
    {
        /*draw a battery */
        drawRect(x0, y0 - 19, w, h);				// frame
        fillRect(x0 + w, y0 - 17, w / 6, h / 2); // plus pin

        /* color goes from tftGreen to tftRed according to the battery charge level */
        fillRect(x0 + 2, y0 - 17, ((w - 4) * percent) / 100, h - 4); // fill the battery body with the color matching the charge level
    }
}

void Display2::drawFreq(uint8_t freqIndex)
{
    std::array<const char*, 7> freqLabels{"A0", "B0", "C0", "D0", "E0", "F0", "G0"};
    
    setTextSize(1.0);
    setFont(&fonts::FreeSansBold24pt7b);
    fillRect(10, 25, 70, 40, TFT_BLACK);
    drawString(freqLabels.at(freqIndex), 10, 25);
}
void Display2::drawLight()
{
    setTextSize(1.0);
    setFont(&fonts::FreeSansBold12pt7b);
    drawString("ON", 80, 43);
}

void Display2::eraseLight()
{
    fillRect(80, 38, 45, 24, TFT_BLACK);
}

void Display2::drawMode(char const *mode)
{
    setTextSize(1.0);
    fillRect(10, 5, 15, 15, TFT_BLACK);
	setFont(&fonts::FreeSansBold9pt7b);
	drawString(mode, 10, 5);
}
void Display2::drawBatteryEmpty()
{
    clear();

    setTextSize(1.0);
    setFont(&fonts::FreeSansBold9pt7b);

    const char *msg1{"Batterie faible !"};
	drawString(msg1, (getPanel()->width() - textWidth(msg1)) / 2, 10);

	const char *msg2{"Mise en"};
	drawString(msg2, (getPanel()->width() - textWidth(msg2)) / 2, 30);

	const char *msg3{"veille."};
	drawString(msg3, (getPanel()->width() - textWidth(msg3)) / 2, 50);
}

void Display2::drawAutoPowerOff()
{
    clear();

    setTextSize(1.0);
	setFont(&fonts::FreeSansBold9pt7b);

    const char *msg1{"Inactivite !"};
	drawString(msg1, (getPanel()->width() - textWidth(msg1)) / 2, 10);

	const char *msg2{"Mise en"};
	drawString(msg2, (getPanel()->width() - textWidth(msg2)) / 2, 30);

	const char *msg3{"veille."};
	drawString(msg3, (getPanel()->width() - textWidth(msg3)) / 2, 50);
}

void Display2::clear() {fillScreen(TFT_BLACK);}
