#pragma once

#define LGFX_USE_V1 // using API V1 which is much easy to use

#include <LovyanGFX.hpp>

class Display2 : public lgfx::LGFX_Device
{
  public:
	explicit Display2();
	virtual ~Display2();

	void drawWelcome(); // splash screen
	void drawGoodbye();
	void drawProgression(int progression); // 0% to 100%
	void drawHealthStatus(bool leftLed, bool rightLed);

	void drawBatteryStatus(int chargePercent, int batteryPercent, bool isCharging, bool isUSB);
	void drawFreq(uint8_t freqIndex);
	void drawLight();
	void eraseLight();
	void drawMode(char const *mode);
	void drawBatteryEmpty();
	void drawAutoPowerOff();

	void clear();

  private:
	lgfx::Panel_SH110x  _panel_instance;
	lgfx::Bus_I2C       _bus_instance;


	int32_t		 lastProgression_{0};
	LGFX_Sprite *freqNames_[7];
};