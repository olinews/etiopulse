#pragma once

#include <map>

#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "freertos/FreeRTOS.h"
#include "freertos/timers.h"

#include "event_loop.h"
#include "sdkconfig.h"

/**
 * @brief Power Supply Unit management class
 *
 * This class monitors the status of the PSU (battery voltage, USB plugged, charge/standby status, ...)
 * If any change in PSU status is detected it triggers an event that HSM can can & process.
 *
 */
class Psu
{
  public:
	struct PsuStatus
	{
		bool isStandby_{false};	 // true if USB plugged & battery full charged
		bool isCharging_{false}; // true if USB plugged & battery charging
		bool isUSB_{false};		 // true if USB plugged
		int	 chargePercent{0};	 // 0 means charge starting; 100 means charge finished; meaningfull only when isUSB_ == true
		int	 batteryPercent{0};	 // 0 mean battery fully discharged; 100 means battery fully charged

		bool operator!=(const PsuStatus &other) const;
	};

	void start();

	explicit Psu(EventLoop &eventLoop);
	
	const PsuStatus &getStatus() const { return currStatus_; }

  private:
	/* convert untyped config params into strongly typed constants */
	static constexpr size_t maxMeasures_{CONFIG_PSU_STATUS_SAMPLES}; // how many measures to acquire to compute the mean voltage
	static constexpr TickType_t statusSamplingPeriod_{pdMS_TO_TICKS(CONFIG_PSU_STATUS_PERIOD_MSEC)}; // status sampling period
	static const uint32_t		mVBattEmpty_{3500};													 // min voltage on battery
	static const uint32_t		mvBattFull_{4200};													 // max voltage on battery
	static const uint32_t		mvProgEnd_{110};	// min voltage on pin TP4056::PROG
	static const uint32_t		mVProgBegin_{1000}; // max voltage on pin TP4056::PROG

	/* ADC configuration variables */
	const int					   adcVref_{1100};
	const adc_unit_t			   adcUnit_{ADC_UNIT_1};
	adc1_channel_t				   adcBattSenseChannel_{static_cast<adc1_channel_t>(CONFIG_BATT_SENSE)};
	adc1_channel_t				   adcChargeSenseChannel_{static_cast<adc1_channel_t>(CONFIG_CHARGE_SENSE)};
	esp_adc_cal_characteristics_t *adcBattCalibration_{nullptr};
	esp_adc_cal_characteristics_t *adcChargeCalibration_{nullptr};

	PsuStatus	  lastStatus_; // to compare with currStatus_ to decide if we trigger an event
	PsuStatus	  currStatus_; // the most recent status we have
	TimerHandle_t statusTimer_;
	EventLoop	  eventLoop_;

	uint32_t mvBatts_[maxMeasures_]; // Last ten measures of the battery voltage (mV)
	uint32_t mvProgs_[maxMeasures_]; // Last ten measures of the charge_sense (mV)
	int		 measureIndex_{0};		 // index in the two previous arrays

	static void statusCallback_(TimerHandle_t arg) { static_cast<Psu *>(pvTimerGetTimerID(arg))->onStatus_(); }

	void onStatus_();
};