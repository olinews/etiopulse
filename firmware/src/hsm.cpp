#include "esp_err.h"
#include "esp_sleep.h"
#include "log_level.h"

#include "hsm.h"

Hsm::Hsm(EventLoop *eventLoop) : psu_(*eventLoop), buttons_(*eventLoop), eventLoop_(eventLoop), freqScanner_(*eventLoop)
{
	display_.init();
	display_.fillScreen(TFT_BLACK);

	healthCheck_();

	manualState_ = new ManualState(this);
	ScanReadyState_ = new ScanReadyState(this);
	scanRunningState_ = new ScanRunningState(this);
	baseState_ = new BaseState(this);
	psu_.start();
}

Hsm::~Hsm()
{
	display_.drawGoodbye();
	vTaskDelay(pdMS_TO_TICKS(3000));
	display_.clear(); // this is to prevent ghost display at startup

	powerLed_.lightOff();
}

void Hsm::healthCheck_()
{
	display_.drawWelcome();
	display_.drawProgression(0);
	vTaskDelay(pdMS_TO_TICKS(500));
	buttons_.calibrate();
	display_.drawProgression(34);
	vTaskDelay(pdMS_TO_TICKS(500));
	auto leftLedHealthy{false};
	leftLedHealthy = powerLed_.checkIrLed(static_cast<gpio_num_t>(CONFIG_IR_SENSE_LEFT));
	ESP_LOGI(__FILE__, "%s", (leftLedHealthy ? "Left LED is good!" : "/!\\Left LED not working!"));
	display_.drawProgression(67);
	vTaskDelay(pdMS_TO_TICKS(500));

	auto rightLedHealthy{false};
	rightLedHealthy = powerLed_.checkIrLed(static_cast<gpio_num_t>(CONFIG_IR_SENSE_RIGHT));
	ESP_LOGI(__FILE__, "%s", (rightLedHealthy ? "Right LED is good!" : "/!\\Right LED not working!"));
	display_.drawProgression(100);
	vTaskDelay(pdMS_TO_TICKS(500));
	
	display_.clear();

	if (!leftLedHealthy || !rightLedHealthy)
	{
		display_.drawHealthStatus(leftLedHealthy, rightLedHealthy);
		vTaskDelay(pdMS_TO_TICKS(4000));
		display_.clear();
	}
}

Hsm::BaseState::BaseState(Hsm *hsm) : hsm_(hsm)
{
	hsm_->display_.drawFreq(hsm_->powerLed_.getCurrentFreqIndex());
	hsm_->display_.drawMode("M");
	hsm_->nextState_(hsm_->manualState_);
}

void Hsm::BaseState::on_BTN_OK_LP(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_BTN_OK_LP"); }

void Hsm::BaseState::on_BTN_OK_SP(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_BTN_OK_SP"); }

void Hsm::BaseState::on_BTN_DOWN_SP(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_BTN_DOWN_SP"); }

void Hsm::BaseState::on_BTN_UP_SP(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_BTN_UP_SP"); }

void Hsm::BaseState::on_BTN_MODE_SP(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_BTN_MODE_SP"); }

void Hsm::BaseState::on_BTN_LEFT_LP(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_BTN_LEFT_LP"); }

void Hsm::BaseState::on_BTN_RIGHT_LP(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_BTN_RIGHT_LP"); }

void Hsm::BaseState::on_BTN_MODE_LP(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_BTN_MODE_LP"); }

void Hsm::BaseState::on_END_OF_SCAN(EventBits_t event) { ESP_LOGI(__FILE__, "BaseState::on_END_OF_SCAN"); }

void Hsm::BaseState::on_BATTERY_STATUS(EventBits_t event)
{
	ESP_LOGI(__FILE__, "BaseState::on_BATTERY_STATUS");
	auto status{hsm_->psu_.getStatus()};
	if (!status.isCharging_ && status.batteryPercent <= 10)
	{ // Battery empty
		ESP_LOGI(__FILE__, "Battery is empty");
		hsm_->display_.drawBatteryEmpty();
		vTaskDelay(pdMS_TO_TICKS(3000));
		hsm_->powerLed_.lightOff();
		hsm_->display_.clear(); // this is to prevent ghost display at startup
		hsm_->eventLoop_->setEvents(EventLoop::BTN_OK_LP);
	}
	else
	{ // Battery normal
		hsm_->display_.drawBatteryStatus(status.chargePercent, status.batteryPercent, status.isCharging_, status.isUSB_);
	}
}

void Hsm::BaseState::on_AUTO_POWER_OFF(EventBits_t event)
{
	ESP_LOGI(__FILE__, "BaseState::on_AUTO_POWER_OFF");
	hsm_->display_.drawAutoPowerOff();
	vTaskDelay(pdMS_TO_TICKS(3000));
	hsm_->display_.clear(); // this is to prevent ghost display at startup
	hsm_->powerLed_.lightOff();
	hsm_->eventLoop_->setEvents(EventLoop::BTN_OK_LP);
	// ESP_ERROR_CHECK(rtc_gpio_hold_en(static_cast<gpio_num_t>(CONFIG_IR_DRIVE)));
}

//-----------------------------------------------------------------------------
void Hsm::ManualState::on_BTN_DOWN_SP(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ManualState::on_BTN_DOWN_SP");

	hsm_->powerLed_.prevFreq();
	hsm_->display_.drawFreq(hsm_->powerLed_.getCurrentFreqIndex());
}

void Hsm::ManualState::on_BTN_UP_SP(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ManualState::on_BTN_UP_SP");

	hsm_->powerLed_.nextFreq();
	hsm_->display_.drawFreq(hsm_->powerLed_.getCurrentFreqIndex());
}

void Hsm::ManualState::on_BTN_OK_SP(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ManualState::on_BTN_OK_SP");

	if (hsm_->powerLed_.lightStatus() == 0)
	{ // light is off
		hsm_->powerLed_.lightOn();
		hsm_->display_.drawLight();
	}
	else
	{
		hsm_->powerLed_.lightOff();
		hsm_->display_.eraseLight();
	}
}

void Hsm::ManualState::on_BTN_MODE_SP(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ManualState::on_BTN_MODE_SP");
	hsm_->display_.drawMode("A");
	hsm_->powerLed_.lightOff();
	hsm_->display_.eraseLight();
	hsm_->powerLed_.setFreq(0);
	hsm_->display_.drawFreq(0);
	hsm_->nextState_(hsm_->ScanReadyState_);
}

//-----------------------------------------------------------------------------
void Hsm::ScanReadyState::on_BTN_OK_SP(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ScanReadyState::on_BTN_OK_SP");
	hsm_->nextState_(hsm_->scanRunningState_);
	hsm_->freqScanner_.start();
}

void Hsm::ScanReadyState::on_BTN_MODE_SP(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ScanReadyState::on_BTN_MODE_SP");
	hsm_->display_.drawMode("M");
	hsm_->powerLed_.lightOff();
	hsm_->display_.eraseLight();
	hsm_->powerLed_.setFreq(0);
	hsm_->nextState_(hsm_->manualState_);
}

//-----------------------------------------------------------------------------
void Hsm::ScanRunningState::on_BTN_MODE_SP(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ScanRunningState::on_BTN_MODE_SP");
	hsm_->freqScanner_.stop();
	hsm_->display_.drawMode("M");
	hsm_->powerLed_.lightOff();
	hsm_->display_.eraseLight();
	hsm_->powerLed_.setFreq(0);
	hsm_->display_.drawFreq(0);
	hsm_->nextState_(hsm_->manualState_);
}

void Hsm::ScanRunningState::on_END_OF_SCAN(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ScanRunningState::on_END_OF_SCAN");

	hsm_->freqScanner_.stop();
	hsm_->display_.drawMode("M");
	hsm_->powerLed_.lightOff();
	hsm_->display_.eraseLight();
	hsm_->powerLed_.setFreq(0);
	hsm_->display_.drawFreq(0);
	hsm_->nextState_(hsm_->manualState_);
}

//-----------------------------------------------------------------------------
void Hsm::onEvent(EventBits_t event)
{
	ESP_LOGI(__FILE__, "ScanRunningState::onEvent() %x", event);

	switch (event)
	{
	case EventLoop::BTN_OK_LP:
		currentState_->on_BTN_OK_LP(event);
		break;
	case EventLoop::BTN_OK_SP:
		currentState_->on_BTN_OK_SP(event);
		break;
	case EventLoop::BTN_DOWN_SP:
		currentState_->on_BTN_DOWN_SP(event);
		break;
	case EventLoop::BTN_UP_SP:
		currentState_->on_BTN_UP_SP(event);
		break;
	case EventLoop::BTN_MODE_SP:
		currentState_->on_BTN_MODE_SP(event);
		break;
	case EventLoop::BTN_LEFT_LP:
		currentState_->on_BTN_LEFT_LP(event);
		break;
	case EventLoop::BTN_RIGHT_LP:
		currentState_->on_BTN_RIGHT_LP(event);
		break;
	case EventLoop::BTN_MODE_LP:
		currentState_->on_BTN_MODE_LP(event);
		break;
	case EventLoop::END_OF_SCAN:
		currentState_->on_END_OF_SCAN(event);
		break;
	case EventLoop::BATTERY_STATUS:
		currentState_->on_BATTERY_STATUS(event);
		break;
	case EventLoop::AUTO_POWER_OFF:
		currentState_->on_AUTO_POWER_OFF(event);
		break;
	}
}