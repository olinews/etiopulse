#include "log_level.h"
#include "soc/soc.h"

#include "freq_scanner.h"
#include "sdkconfig.h"

FreqScanner::FreqScanner(EventLoop& eventLoop) : eventLoop_(eventLoop)
{

}

FreqScanner::~FreqScanner()
{

}

void FreqScanner::start()
{
    xTaskCreatePinnedToCore(&FreqScanner::s_scan_, "scan", 2048, this, 9, &scanTask_, PRO_CPU_NUM);
}

void FreqScanner::stop() 
{ 
    if (scanTask_ != nullptr)
    {    
        eventLoop_.setEvents(EventLoop::END_OF_SCAN);
        vTaskDelete(scanTask_);
		scanTask_ = nullptr;
	}
}

void FreqScanner::scan_()
{
#ifdef CONFIG_MODE_SCAN_LOOP_BACK
    do
    {
#endif
		for (int i{0}; i < 7; i++)
		{
            eventLoop_.setEvents(EventLoop::BTN_OK_SP);	    				        // power on current freq
            vTaskDelay(pdMS_TO_TICKS(CONFIG_MODE_SCAN_FREQ_DURATION*1000));         // let the led shine for a while
            eventLoop_.setEvents(EventLoop::BTN_OK_SP);                             // power off current freq
            eventLoop_.setEvents(EventLoop::BTN_UP_SP);                          // next freq
            vTaskDelay(pdMS_TO_TICKS(CONFIG_MODE_SCAN_INTERVAL_DURATION*1000));     // let the patient recover a little bit
        }
#ifdef CONFIG_MODE_SCAN_LOOP_BACK
	} while (CONFIG_MODE_SCAN_LOOP_BACK);
#endif

	eventLoop_.setEvents(EventLoop::END_OF_SCAN);
	scanTask_ = nullptr;
	vTaskDelete(NULL);
}