#include "log_level.h"
#include "sdkconfig.h"

#include "event_loop.h"

EventLoop::EventLoop()
{
	/* init auto power-off watchdog */
	watchdogTimer_ =
		xTimerCreate("auto_power_off",									   // Just a text name, not used by the RTOS kernel.
					 pdMS_TO_TICKS(CONFIG_AUTO_POWER_OFF_WATCHDOG * 1000), // The timer period in ms, must be greater than 0.
					 pdFALSE,		   // The timers will not auto-reload itself when it expire.
					 this,			   // This pointer passed to the callback
					 watchdogCallback_ // Each timer calls the same callback when it expires.
		);
	xTimerStart(watchdogTimer_, 0); // timer start immediately
}

void EventLoop::setEvents(const EventBits_t events)
{
	BaseType_t pxHigherPriorityTaskWoken{pdFALSE};

	xEventGroupSetBitsFromISR(hEventGroup_, events, &pxHigherPriorityTaskWoken);
}

EventBits_t EventLoop::getNextEvent(TickType_t xTicksToWait)
{
	if (eventsPending_ == 0)
	{
		eventsPending_ = xEventGroupWaitBits(hEventGroup_, allEventsMask_, pdTRUE, pdFALSE, xTicksToWait);
	}

	// ESP_LOGD(__FILE__, "alleventsMask=%04x, eventsPending=%04x xTicksToWait=%d", allEventsMask_, eventsPending_,	xTicksToWait);

	if ((watchdogResetEventsMask_ & eventsPending_) != 0)
	{ // need to reset the watchdog & prevent auto power-off
		xTimerReset(watchdogTimer_, 0);
	}

	for (auto &eventMask : eventsMasks_)
	{
		if (eventsPending_ & eventMask)
		{
			eventsPending_ &= ~eventMask; // clear bit of that event
			return eventMask;
		}
	}

	return 0; // no event
}

void EventLoop::onWatchdog_()
{
	ESP_LOGI(__FILE__, "onWatchdog_()");
	setEvents(AUTO_POWER_OFF);
}
