#pragma once

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "event_loop.h"

/**
 * @brief Frequency scanner to manage the mode 'autoscan'
 * 
 * There are two main modes to check frequencies. The first, 'manual' is manager directly by Hsm.
 * The second, 'autoscan', is managed by this class. It will cycle the power led through Nogier's 
 * frequency set sequentially.
 * This mode can be trigger once (from A0 to G0 and back to manual mode) or loop back (from A0 to
 *  G0 then back to A0 and so on).
 */
class FreqScanner
{
public:
    explicit FreqScanner(EventLoop& eventLoop);
    virtual ~FreqScanner();
    void start();
    void stop();

private:
    TaskHandle_t scanTask_{nullptr};
	EventLoop			  eventLoop_;
    
	static void IRAM_ATTR s_scan_(void *arg) { reinterpret_cast<FreqScanner *>(arg)->scan_(); }
	void IRAM_ATTR		scan_();
};