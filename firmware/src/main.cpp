#include "esp_sleep.h"
#include "esp_spi_flash.h"
#include "esp_system.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "log_level.h"

#include "buttons.h"
#include "event_loop.h"
#include "freq_scanner.h"
#include "hsm.h"

extern "C"
{
	extern void app_main();
}

void printChipInformation()
{
	esp_chip_info_t chip_info;
	esp_chip_info(&chip_info);
	ESP_LOGI(__FILE__, "This is %s chip with %d CPU cores, WiFi%s%s, ", CONFIG_IDF_TARGET, chip_info.cores,
			 (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "", (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");
	ESP_LOGI(__FILE__, "silicon revision %d, ", chip_info.revision);
	ESP_LOGI(__FILE__, "%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
			 (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");
	ESP_LOGI(__FILE__, "Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());
}

// TODEL Only for debugging purpose
// void blinkIO26()
// {
//     /* Configure GPIO connected to IR LED PCB */
// 	gpio_config_t ioConfig{
// 		.pin_bit_mask = (1 << CONFIG_IR_POWER) | (1 << CONFIG_IR_DRIVE),
// 		.mode = GPIO_MODE_OUTPUT,
// 		.pull_up_en = GPIO_PULLUP_DISABLE,
// 		.pull_down_en = GPIO_PULLDOWN_DISABLE,
// 		.intr_type = GPIO_INTR_DISABLE
// 	};
// 	ESP_ERROR_CHECK(gpio_config(&ioConfig));
// 	gpio_set_level(static_cast<gpio_num_t>(CONFIG_IR_POWER), 0);
// 	gpio_set_level(static_cast<gpio_num_t>(CONFIG_IR_DRIVE), 0);

// 	gpio_set_level(static_cast<gpio_num_t>(CONFIG_IR_POWER), 1);
//     while(1)
//     {
// 	    gpio_set_level(static_cast<gpio_num_t>(CONFIG_IR_DRIVE), 1);
//         vTaskDelay(pdMS_TO_TICKS(8));
// 	    gpio_set_level(static_cast<gpio_num_t>(CONFIG_IR_DRIVE), 0);
//         vTaskDelay(pdMS_TO_TICKS(24));
//     }
// }

bool wakeUpActually()
{
	bool goodReasonToWakeUp{false};
	ESP_LOGI(__FILE__, "IN esp_sleep_get_wakeup_cause");
	auto wakeUpCause{esp_sleep_get_wakeup_cause()};
	ESP_LOGI(__FILE__, "OUT esp_sleep_get_wakeup_cause");
	switch (wakeUpCause)
	{
	case ESP_SLEEP_WAKEUP_UNDEFINED:
		/* after upload */
		ESP_LOGI(__FILE__, "Wake-up reason is ESP_SLEEP_WAKEUP_UNDEFINED");
		goodReasonToWakeUp = true;
		break;

	case ESP_SLEEP_WAKEUP_TOUCHPAD: {
		ESP_LOGD(__FILE__, "IN esp_sleep_get_touchpad_wakeup_status");
		touch_pad_t padNum{};
		touch_pad_get_wakeup_status(&padNum);
		ESP_LOGD(__FILE__, "OUT esp_sleep_get_touchpad_wakeup_status");
		ESP_LOGI(__FILE__, "Wake-up reason is ESP_SLEEP_WAKEUP_TOUCHPAD by touch_pad %d", padNum);
		goodReasonToWakeUp = true;
		break;
	}

	case ESP_SLEEP_WAKEUP_EXT0:
		ESP_LOGI(__FILE__, "Wake-up reason is ESP_SLEEP_WAKEUP_EXT0");
		break;
	case ESP_SLEEP_WAKEUP_EXT1:
		ESP_LOGI(__FILE__, "Wake-up reason is ESP_SLEEP_WAKEUP_EXT1");
		break;
	case ESP_SLEEP_WAKEUP_TIMER:
		ESP_LOGI(__FILE__, "Wake-up reason is ESP_SLEEP_WAKEUP_TIMER");
		break;
	case ESP_SLEEP_WAKEUP_ULP:
		ESP_LOGI(__FILE__, "Wake-up reason is ESP_SLEEP_WAKEUP_ULP");
		break;
	case ESP_SLEEP_WAKEUP_GPIO:
		ESP_LOGI(__FILE__, "Wake-up reason is ESP_SLEEP_WAKEUP_GPIO");
		break;
	case ESP_SLEEP_WAKEUP_UART:
		ESP_LOGI(__FILE__, "Wake-up reason is ESP_SLEEP_WAKEUP_UART");
		break;
	default:
		break;
	}

	return goodReasonToWakeUp;
}

void enableTouchPadWakeUp()
{
	/* Initialize touch pad peripheral, it will start a timer to run a filter */
	ESP_LOGD(__FILE__, "Initializing touch pad");
	ESP_LOGD(__FILE__, "IN touch_pad_init");
	ESP_ERROR_CHECK(touch_pad_init());
	ESP_LOGD(__FILE__, "OUT touch_pad_init");

	/* If use touch pad wake up, should set touch sensor FSM mode at 'TOUCH_FSM_MODE_TIMER'. */
	ESP_LOGD(__FILE__, "IN touch_pad_set_fsm_mode");
	touch_pad_set_fsm_mode(TOUCH_FSM_MODE_TIMER);
	ESP_LOGD(__FILE__, "OUT touch_pad_set_fsm_mode");

	/* Set reference voltage for charging/discharging
	   For most usage scenarios, we recommend using the following combination:
	   the high reference voltage will be 2.7V - 1V = 1.7V, The low reference
	   voltage will be 0.5V. */
	ESP_ERROR_CHECK(touch_pad_set_voltage(TOUCH_HVOLT_2V4, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V));

	/* enable wake-up with touchpad */
	ESP_ERROR_CHECK(esp_sleep_enable_touchpad_wakeup());
}

void app_main()
{
	enableTouchPadWakeUp();

	if (wakeUpActually())
	{
		printChipInformation();
		printf("Etiopulse v%s", "21.01\n");

		EventLoop	eventLoop;
		Hsm			hsm(&eventLoop);

		/* main message loop running from PRO_CPU */
		while (true)
		{
			auto event = eventLoop.getNextEvent(pdMS_TO_TICKS(60000));
			if (event == EventLoop::BTN_OK_LP)
			{
				break;
			}

			ESP_LOGD(__FILE__, "event %04x", event);
			if (event)
			{
				hsm.onEvent(event);
			}
		}
	}
	else
	{
		ESP_LOGD(__FILE__, "Not a good reason to wake-up. Going back to deep sleep.");
		// ESP_ERROR_CHECK(rtc_gpio_hold_en(static_cast<gpio_num_t>(CONFIG_IR_DRIVE)));
	}

	ESP_LOGD(__FILE__, "Going to deep sleep.");
	esp_deep_sleep_start();
}